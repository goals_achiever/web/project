class CreateGoals < ActiveRecord::Migration[6.0]
  def change
    create_table :goals do |t|
      t.references :user, null: false, index: true

      t.string :name, null: false

      t.timestamps null: false
    end
  end
end
