class CreateResources < ActiveRecord::Migration[6.0]
  def change
    create_table :resources do |t|
      t.references :template, index: { name: 'resources_tt_and_ti' }, polymorphic: true
      t.references :owner, polymorphic: true

      t.integer :value

      t.timestamps null: false

      t.index %i[owner_id owner_type], name: 'resources_oi_and_ot'
      t.index %i[template_id owner_id owner_type], unique: true, name: 'resources_rti_and_oi_and_ot'
    end
  end
end
