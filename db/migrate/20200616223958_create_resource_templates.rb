class CreateResourceTemplates < ActiveRecord::Migration[6.0]
  def change
    create_table :resource_templates do |t|
      t.string :name, null: false, index: { unique: true }
      t.string :type, null: false

      t.timestamps null: false
    end
  end
end
