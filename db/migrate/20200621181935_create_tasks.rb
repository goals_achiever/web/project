class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks do |t|
      t.references :goal, null: false, index: true

      t.string :name, null: false
      t.boolean :completed, null: false, default: false

      t.timestamps null: false
    end
  end
end
