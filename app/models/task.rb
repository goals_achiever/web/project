# frozen_string_literal: true

class Task < ApplicationRecord
  belongs_to :goal

  has_many :resources, as: :owner

  validates :name, presence: true
end
