# frozen_string_literal: true

class Resource < ApplicationRecord
  belongs_to :template, class_name: 'Resource::Template', polymorphic: true
  belongs_to :owner, polymorphic: true
end
