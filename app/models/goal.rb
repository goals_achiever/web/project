# frozen_string_literal: true

class Goal < ApplicationRecord
  belongs_to :user

  has_many :tasks
  has_many :resources, as: :owner

  validates :name, presence: true
end
