# frozen_string_literal: true

class Resource::Template < ApplicationRecord
  has_many :resources

  validates :name, presence: true
  validates :type, presence: true
end
